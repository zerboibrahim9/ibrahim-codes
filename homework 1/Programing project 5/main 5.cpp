   //* Name: Minwossi Zerbo
   //* Student ID: 2498114 
   //* Date: 27/02/2014
   //* HW:Project 5
   //* Problem: Program that Reads two integers and outputs their sum and product
   //* I certify this is my own work and code
#include <cstdlib>
#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    int integer_A, integer_B, total_integers, product_integers;
	
	cout << "HELLO AND WELCOME.\n";
	cout << "Enter the first integer A of your choice:\n";
	cin >> integer_A;
	cout << "Enter the second integer B of your choice:\n";
	cin >> integer_B;
	total_integers = integer_A + integer_B;
	product_integers = integer_A * integer_B;
	cout << "The sum of your two integers A and B is:\n";
	cout << total_integers << endl ;
	cout << " and Their product is:\n";
	cout << product_integers << endl ;
	cout << "This is the end of the program.\n";
	
    system("PAUSE");
    return EXIT_SUCCESS;
}
