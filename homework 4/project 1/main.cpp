    //* Name: Minwossi Zerbo
   //* Student ID: 2498114 
   //* Date: 03/19/2014
   //* HW 4:Project 1
   //* Problem: program that reads in 10 numbers and output the sum of negative
      // number, positives numbers and the average of each of them
   //* I certify this is my own work and code
   
   
#include <cstdlib>
#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    int numPositive = 0;
    int numNegative = 0;
    double average1;
    double average2;
    
    for (int i = 0; i < 10; i++)
    {
        cout << "Enter any number you want: " << endl;
        int num;
        cin >> num;
        
        if ( num > 0) 
        {
             numPositive += num;
             }
        else 
        {
             numNegative += num;
             }
    }
    cout << "The sum of your positive numbers is: " << numPositive << endl;
    cout << "The sum of your negative numbers is: " << numNegative << endl;
    
    average1 = numPositive / 10.00;
    average2 = numNegative / 10.00;
             
    cout << "The average of your positive numbers is: " << average1 <<endl;
    cout << "The average of your negative numbers is: " << average2 << endl;         
             
    system("PAUSE");
    return EXIT_SUCCESS;
}
