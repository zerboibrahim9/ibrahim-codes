    //* Name: Minwossi Zerbo
   //* Student ID: 2498114 
   //* Date: 03/06/2014
   //* HW 2:Project 1
   //* Problem: Program which plays the game of Mad lib
   //* I certify this is my own work and code
   
   
#include <cstdlib>
#include <iostream>
#include <iomanip>


using namespace std;

int main(int argc, char *argv[])
{
    string name1, name2, food, adjective, color, animal;
    int number;
    
    cout << " Enter a two names, a food, a number, an adjective, "
         << " a color, and an animal : " << endl;
    cin >> name1
        >> name2
        >> food
        >> number
        >> adjective
        >> color
        >> animal;
        
    cout << " Dear " << name1 << endl 
         << "I am sorry that I am unable to turn in my "
         << "homework at this time. First, I ate a rotten " << food 
         << " which made me turn " << color 
         << " and extremely ill. I came down with" 
         << " a fever of " << number << "." 
         << "Next, my " << adjective 
         << " pet " << animal << " must have smelled the remains of the " <<food
         << "on my homework because he ate it." 
         << "I am currently rewriting my "
         << "homework and hope you will  " 
         << "accept it late."<< endl << endl
         << " Sincerely," << endl
         << " " << name2 << endl;

    
    
    system("PAUSE");
    return EXIT_SUCCESS;
}
