/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 21, 2014, 4:06 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
     cout << "Enter a character" << endl;
    
    char c;
    cin.get(c);
    
    cout << "Your character was: " << c << endl;
    
    // use  getline to get a sentence from the user
    
    cout << "Please enter a sentence" << endl;
    
    string sentence;
    
    //getline needs a buffer and string as argument
    //void function
    
    getline(cin, sentence);
    
    cout <<" Your sentence is: " << sentence << endl;

    //use the stream insertion operator
    // >> reads until white space or a new line, and then
    // ignore the character
    
    string word;
    cout << "Please enter a word" << endl;
    cin >> word;
    
    cout <<"Your word was: " << word << endl;
    
    //break my cin buffer
    
    while(true)
    {
        int number;
        cout <<"Enter a number" << endl;
        cin >> number;
        cout << "Your number was: " << number << endl;
        
        if(cin.fail())
        {
            
        cin.clear();
        cin.ignore(256, '\n');
        
        }
    }
    
    return 0;
}

