/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 5, 2014, 4:32 PM
 */

#include <cstdlib>
#include<iostream>
#include<iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int num1, num2;
    int stock;
    
    cout << " Enter two numbers of your choice : " << endl;
    cin  >> num1 >> num2;
 cout << " X = " << setw(4)<< right << num1 << " Y = " << num2 << endl;
    cout << setfill('-') << setw(60) << " " << left << endl;
    
    stock = num1;
    num1 = num2;
    num2 = stock;
    
   
cout << " X = " << setw (4) << num1 <<" Y = " << num2 << endl;

    return 0;
}

