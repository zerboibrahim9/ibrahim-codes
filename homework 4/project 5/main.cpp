//* Name: Minwossi Zerbo
   //* Student ID: 2498114 
   //* Date: 03/19/2014
   //* HW 4:Project 5
   //* Problem: grade program
   //* I certify this is my own work and code

#include <cstdlib>
#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    int numExercise;
    int grade;
    int total;
    int totalGrade = 0;
    int totalTotal = 0;
    double average;
   
    
    cout << "How many exercises do you want to input? " << endl;
    cin >> numExercise;
    int count = 0;
    for(int i = 0; i < numExercise; i++)
    {
            count++;
            cout << " Score reiceived for exercise " << count << " : " << endl;
            cin >> grade;
            cout << " Total points possible for exercise " 
                 << count << " : " << endl;
            cin >> total;
            totalGrade = totalGrade + grade;
            totalTotal = totalTotal + total;
            average = (static_cast <double> (totalGrade) / totalTotal) * 100;
            
            }
   cout << " Your total points is " << totalGrade 
        << " out of " << totalTotal
        << " and your average is " << average << "%" << endl;
   
    system("PAUSE");
    return EXIT_SUCCESS;
}
