/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 2, 2014, 4:56 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;
/*
 * 
 */
// global constant
const double PI = 3.14;


//function prototype for circumference and area
//prototype  do not need variable
double circumference(double);
double area(double);

//swap functions
void swapNonReferenced(int, int);
void swapReferenced(int&, int&);
void output(int, int);
/*
 * 
 * 
 */

int main(int argc, char** argv) {
    
    // prompt the user
    
    cout << "Please enter a radius:" << endl;
    int radius;
    cin >> radius;
   
    cout << "The area is: " << area(radius) << endl;
    cout << "The circomference is: " << circumference(radius) << endl;

    cout << " Please enter two integers: " << endl;
    int num1, num2;
    cin >> num1 >> num2;
    
            cout << endl <<"Using non-referenced" << endl;
    output(num1, num2);
    swapNonReferenced(num1, num2);
    output(num1, num2);
            cout << endl <<"Using referenced" << endl;
    output(num1, num2);
    swapReferenced(num1, num2);
    output(num1, num2);
    
    return 0;
}

/*
 * three main topics to address
 * 1. purpose (high level overview of function)
 * 2. return value (what it represent)
 * 3. parameters (significance of them)
 * 
 * 1.Circumference function calculates the circumference of a circle
 * with a given radius.The circumference is  two * pi * radius
 * 
 * 2.returns a double representing the circumference of a circle
 * 
 * 3.One parameter. a double representing the radius of the circle
 */
 
double circumference(double r)
{
    return 2 * PI * r;
}

/*
 * 1. The area function returns the area of a circle 
 * with a given radius
 * 
 * 2. returns a double, representing the area
 * 
 * 3. takes one parameter as a double called radius,
 * representing the radius of a circle
 */

double area(double radius)
{
    return PI * radius * radius;
}

/*
 *  swapNonreferend attempts to swap two values of type integers
 * 
 * the function returns nothing.
 */
void swapNonreferenced(int x, int y)
{
    int temp = x;
    x = y;
    y = temp;
}

/*
 *  swapReferend attempts to swap two values of type integers
 * 
 * the function returns nothing.
 */
void swapReferenced(int&x, int&y)
{
    int temp = x;
    x = y;
    y = temp;
}

/*
 * Output function outputs two variable of type double
 */

void output(int x, int y)
{
    cout << "First: " << x;
    cout << "Second: " << y
         << endl;   
    
}