/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 5, 2014, 4:09 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    double purchase;
    double change;
    int payment;
    int dollars, quarters , dimes, nickels, pennies;
    
    cin >> purchase;
    cout << " Your item price is :" << purchase << endl;
    cout << " How much do you have? " << endl;
    cin >> payment;
    
    change =  payment - purchase;
    
    dollars = change / 1;
    quarters = (static_cast <int>(change) % 1)/ 0.25;
    dimes = (quarters % 1 ) / 0.10;
    nickels = (dimes % 1 ) / 0.05;
    pennies = (nickels %1 ) / 0.01;
    cout << change << endl
        << dollars << endl
        << quarters << endl
        << dimes << endl
        << nickels << endl
        << pennies << endl;
    
    

    return 0;
}

