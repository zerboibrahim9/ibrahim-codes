/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 5, 2014, 4:12 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
     int singles, doubles, triples, home_runs, at_bats;
    float slugging_percentage;
    
    cout << "Enter the number of singles, doubles, triples, home_runs and "
         << "at_bats of your " << endl
         << "baseball game" << endl;
    
    cin >> singles
        >> doubles
        >> triples
        >> home_runs
        >> at_bats;
        
    slugging_percentage = ((singles + (2 * doubles) + (3 * triples) + (4 *
         home_runs)) / at_bats;
         
    cout << " The slugging percentage of your hitter is "
         << slugging_percentage << " %" << endl;    

    return 0;
}

