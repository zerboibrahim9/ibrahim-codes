/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 26, 2014, 3:29 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    //declare two integer variables
    int num1, num2;
    
    //junk values
    cout << num1 << " " << num2 << endl;
    
    //initialize my variables
    num1 = num2 = 0;
    
    //should expect 0 0
     cout << num1 << " " << num2 << endl;
     
     //want to get two values from the user
     // Prompt the user when we want to input
     
      cout << "Enter two integers: " << endl;
      
      // user input
      cin >> num1 >> num2;
      
       cout << num1 << " " << num2 << endl;
       
       //calculate the average of two numbers
       
       //calculate total
       int total = num1 + num2;
       
       //calculate average with integer division
       // calculate average with double division
       // calculate average with static casting
       double averageIntDivision = total / 2;
       double averageDoubleDivision = total / 2.0;
       double averageStaticCast = 
                   static_cast<double>(total) / 2;
       
       cout << "Average with int division: "
               << averageIntDivision << endl;
       cout << "Average with double division: "
               << averageDoubleDivision << endl;
       cout << "Average with Static cast: "
               << averageStaticCast << endl;
    
    return 0;
}

