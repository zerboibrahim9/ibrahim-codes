/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 10, 2014, 4:30 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int grade;
    
    cout << " Enter your grade : " << endl;
    cin >> grade;
    
    if( grade >= 90)
    {
        cout << "You have an A";
    }
    else if (grade >= 80)
    {
        cout << "You have a B";
    }
    else if (grade >= 70)
    {
        cout << "You have a C";
    }
    else if (grade >= 60)
    {
        cout << " You have a D";
       
    }
    
    else
        cout << "You failed the class";
    return 0;
}

