/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 24, 2014, 3:22 PM
 */

#include <cstdlib> // cstdlib is unneeded
#include <iostream> // iostream is for input and output from console

// std is short for standard
//all my libraries  comes from the standard C++ namespace
// Also defines context of key words
using namespace std;


// In C++ white spaces are ignored


/*
 * 
 */
// All the codes are located inside main
// There is only one main per program
// Code is executed top to bottom, left to right
// Argc and Argv come  from outside the program
int main(int argc, char** argv) {
    
    // Variable definition
    // Data type string
    // variable name is a message
    // assigning "Hello World" to message
    //string message = "Hello World";

    // All statements end with a semi colon
    // Cout means console outpout
    // << is the screen operator
    // "Hello World" is a string literal
    // endl makes a new line
    //cout << "Hello world" << endl;
    
    string message; // variable declaration
    // Don't hav eto initialize message because i'm using cin to output
    message = "Hello World"; // variable initialization
    
    // Prompt the user
    cout << "Please enter your word" << endl;
    
    // Cin gets input console and stored into the variable message
    cin >> message;
       // Output the variable "message"
       cout << "You entered " << message << endl;
    
    // If codes gets here, then it executed correctly
    // Return flag
    return 0;
}
// All codes is inside main curly brackets
