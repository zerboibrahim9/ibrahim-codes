/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 19, 2014, 3:34 PM
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <ctype.h>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    // create an ifstream object/ declare a variable
    ifstream infile;
    
    //step 1: open file
    infile.open("data.bat");
    // check if file exist
    if (infile.fail())
    {
        
        
        //get user input good file
        
        do 
        {
            cout << "The file failed. Enter a new one"
                << endl;
            string fileName;
            cin >> fileName;
            //open the file
            infile.open(fileName.c_str());
        }while (!infile);
    } 
    
    //file exist read contents form file
    // cout the number of words inside
    int counter = 0;
    while (!infile.eof())
    {
        counter++;
        string word;
        infile >> word;
        cout << "The word is : " << counter 
                << " is: "<<word << endl;
    }
    
    //netsed for loop stair structure
    cout << "Enter a number: " << endl;
    int num;
    cin >>num;
    
    for (int i=0; i < num; i++)
    {
        for (int j = 0; j < num - i; j++)
        {
            cout << "*";
        }
        cout << endl;
    }
    
    // string acces with for loops
    
    cout << "Enter a string: " << endl;
    string word;
    cin >> word;
    
    // change al characters to lower case
    for (int i = 0; i < word.size(); i++)
    {
        word[i] = tolower(word[i]);
    }
    cout << "Lower case word is : " << word;
    
    return 0;
}

