/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 24, 2014, 4:13 PM
 */

#include <cstdlib>
#include <iostream>


using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int num;
    
         
    while (num != -1)
    {
        int thousands = num / 1000;
        num %= 1000;
        int hundreds = num  / 100;
        num %= 100;
        int tens = num / 10;
        num %= 10;
        int ones = num / 1;
        num %= 1;
        //switch on the thoousands
        
        switch (thousands)
        {
            case 3: cout << "MMM"; break;
            case 2: cout << "MM"; break;
            case 1: cout << "M"; break;
            default: cout << "Error"; break ;
        
        
        }
        switch (hundreds)
        {
            case 9: cout << "CM"; break;
            case 8: cout << "DCCC"; break;
            case 7: cout << "DCC"; break;
            case 6: cout << "DC"; break;
            case 5: cout << "D"; break;
            case 4: cout << "CD"; break;
            case 3: cout << "CCC"; break;
            case 2: cout << "CC"; break;
            case 1: cout << "C"; break;
            
            
            
        }
        switch (tens)
        {
            case 9: cout << "XC"; break;
            case 8: cout << "LXXX"; break;
            case 7: cout << "LXX"; break;
            case 6: cout << "LX"; break;
            case 5: cout << "L"; break;
            case 4: cout << "XL"; break;
            case 3: cout << "XXX"; break;
            case 2: cout << "XX"; break;
            case 1: cout << "X"; break;
            
            
            
        }
         switch (ones)
        {
            case 9: cout << "IX"; break;
            case 8: cout << "VIII"; break;
            case 7: cout << "VII"; break;
            case 6: cout << "VI"; break;
            case 5: cout << "V"; break;
            case 4: cout << "IV"; break;
            case 3: cout << "III"; break;
            case 2: cout << "II"; break;
            case 1: cout << "I"; break;
            
            
            
        }
        cout << endl << "Enter a number between 1000-3000 : " << endl
             << "Enter -1 to quit" << endl;
         cin >> num;
         
      }

    return 0;
}

