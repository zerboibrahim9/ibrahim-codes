/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 3, 2014, 5:11 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    int test1, test2, test3, average;
    
    cout << "Enter your first test score : " << endl;
    cin >> test1;
     cout << "Enter your second test  : " << endl;
    cin >> test2;
     cout << "Enter your third test score : " << endl;
    cin >> test3;
    
    average = (test1 + test2 + test3) / 3;
    
    cout << "your average is " << average << endl;
            

    return 0;
}

