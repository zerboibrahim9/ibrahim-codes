/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 17, 2014, 3:29 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    ifstream infile;
    
//    // 3 step process
//    // step 1 open file
//    infile.open("data.dat");
//    
//    //step 2: read from file
//    string word;
//    infile >> word;
//    
//    cout << "The word is : " << word << endl;
    
    // file i/o with while loop
    string word;
    //while loops kep looping until no more input
    
    while (infile >> word)
    {
        cout << "The word is: " << word << endl;
    }
    
    //step 3: close the file
    infile.close();
    
    //file output
    ofstream outfile;
    
    //step 1: open file
    outfile.open("test.txt");
    
    //step2: write to file
    outfile << "Minwossi Zerbo" << endl;
    outfile << "Are you doing good today?";
    
    //step 3: close file
    outfile.close();

    return 0;
}

