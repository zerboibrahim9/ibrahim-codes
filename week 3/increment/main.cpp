/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 3, 2014, 4:59 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int num1 = 6  ;
    cout << "num1 is " << num1 << endl;
    cout << "incrementation by 5 " << endl;
    
    num1 += 5;
    
    cout << "A is now " << num1 << " Because it was incremented by 5 ";

    return 0;
}

