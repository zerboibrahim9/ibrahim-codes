/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 28, 2014, 2:38 PM
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>

using namespace std;
// Create a function that outputs a given array
// function needs the array and size

void output(int a[], int size)
{
    for (int i = 0; i < size; i++)
    {
        cout << a[i] <<  " "  ;
    }
    cout << endl;
}

void output(const vector<int> &v)
{
    for (int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
    
}
/*
 * 
 */
int main(int argc, char** argv) {

        // declare an array with a known size
    int array1[10];
    
    // output the array memory location
    cout << array1 << endl;
    
    // Output value at location 0
    cout << array1[0] << endl;
    
    // Declare an array with a set of values
    
    int array2[] = {1, 2, 3, 4, 5};
    
    //Call the output function
    // If 6 is used as size, causes a bounce error
    output(array2, 5);
     
    
    int array3[3] = {1, 2 ,3};
    output(array3, 5);
   
    
//    int num;
//    cout << "Enter a number" << endl;
//    cin >> num;
//    
//    int array4[num];
//    output(array4, num);
    
    vector<int> v;
    v.push_back(3);
    v.push_back(6);
    v.push_back(8);
    output(v);
    return 0;
}

