#include <cstdlib>
#include <iostream>

using namespace std;

double fonction(double num1, double num2)
{
     
    return num1 / num2 ;
    
     }

int main(int argc, char *argv[])
{
    double liters;
    double mileage;
    double consomation;
    const double CONV = 0.264179;
    // 1 liter == 0,264179 gallons
    cout << "Enter 1 to start or -1 to stop your program " << endl;
    int var;
    cin >> var;
    while (var != -1)
    {
    cout << "Enter the number of liters consumed by your car :" << endl;
    cin >> liters;
    cout << "Enter the number of miles your car traveled :" << endl;
    cin >> mileage;
    
    double convertion;
    convertion = liters * CONV ;
    
    consomation = fonction(mileage, convertion);
    
    cout << "Your car consomation is " << consomation
         << " miles per gallon. " << endl;
         
         cout << "Enter 1 to continue and -1 to end your program " << endl;
         cin >> var;
}
    
    system("PAUSE");
    return EXIT_SUCCESS;
}
