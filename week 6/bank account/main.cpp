/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 24, 2014, 5:07 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    double balance;
    double interest1; 
    double interest2;
    double interest3;
    double newBalance;

    
    cout << " Enter your Account balance" << endl;
    cin >> balance;
    
    if (balance > 1000)
    {
        interest1 = (1.5 * 1000) / 100;
        double num = balance - 1000;
        interest2 = (1 * num) / 100;
    }
    else
    {
        interest3 = (1 * balance) / 100;

    }
    
    double totalInterest = interest1 + interest2;
    
    newBalance = interest1 + interest2 + balance;
    
    cout << "The interest owe is " << totalInterest << endl;
    cout << "Your new balance is " << newBalance << endl;
    
    if ( newBalance <= 10)
    { 
        cout << "The minimum payment is " << newBalance;
    }
    
    else
    {
        double MinPay = (newBalance * 0.1);
        cout << "The minimum payment is " << MinPay << endl;
    }

    return 0;
}

