/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 12, 2014, 3:21 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    // Determine if a number is positive/negative
    // Dtetermine if number is even/odd
    
    cout << " Enter a number: " << endl;
    int num;
    cin >> num;
    
    // first way - without nested statements
    
    if (num < 0)
    {
        cout << "Negative number :" << endl;
    }
    else
    {
        cout << "Positive number :" << endl;
    }
    
    // Check if number is even or odd
    
    if (num % 2 == 0)
    {
        cout << "Even number" << endl;
    }
    else
    {
        cout << "Odd number" << endl;
    }
    
    // Nested statement
    
    if (num < 0)
    {
        cout << "Negative number" << endl;
                
        if (num % 2 ==0)
        {
              cout << "Even number" << endl;
        }       
    
        else
        {
             cout << "Odd number" << endl;
        }
    }   
        else   
        {
             cout << "Positive number" << endl;
                
        if (num % 2 ==0)
        {
              cout << "Even number" << endl;
        }       
    
        else
        {
             cout << "Odd number" << endl;
        }
        
        }
    
    for (int i = 0; i < 100; i++)
    {
        cout << "i: " << i << endl;
    }
    
    //user control loop
    // prompt the suer to enter a number
    // -1 to quit entering
    
    cout << "Enter a number. Enter -1 to stop" << endl;
    int usernum;
    cin >> usernum;
    
    while (usernum != -1)
    {
        // output number to user
        cout << "You entered: " << usernum << endl;
        
        // prompt the user for another number
        cout << "Please enter another number."
                <<" Enter -1 to stop" << endl;
        cin >> usernum;
    
    }
        
    return 0;
}

