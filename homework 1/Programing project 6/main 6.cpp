   //* Name: Minwossi Zerbo
   //* Student ID: 2498114 
   //* Date: 27/02/2014
   //* HW:Project 6
   //* Problem: Programs that output " C S!" in large block letters
   //* I certify this is my own work and code
#include <cstdlib>
#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    	cout << "************************************************" << endl << endl;

      cout << "     c c c            s s s s        ! !" << endl ;
    cout << "   c       c         s        s      ! !" << endl ;
   cout << "  c                 s                ! !" << endl ;
  cout << " c                   s               ! !" << endl ;
  cout << " c                    s s s s        ! !" << endl ;
  cout << " c                            s      ! !" << endl ;
   cout << "  c                            s     ! !" << endl ;
    cout << "   c       c         s        s         " << endl ;
     cout << "     c c c            s s s s        0 0"  << endl << endl  ;
        cout << "************************************************" << endl << endl;
        cout << " Computer Science is cool stuff!!!" << endl ;
        
        
    system("PAUSE");
    return EXIT_SUCCESS;
}
