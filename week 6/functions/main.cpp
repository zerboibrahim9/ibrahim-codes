/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 26, 2014, 3:35 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int square (int num)
{
        return num * num;
}

void problem1()
{
     cout << "Enter an integer: " << endl;
    int value;
    cin >> value;
    
    int squareValue = square(value);
    
    cout << "The square of " << value << "is " << squareValue << endl;
    cout << "The square of " << value << "is " << squareValue << endl;
}
 void endProgram()
     {
          cout << " Your program is ending" << endl;          
        }


int main(int argc, char** argv) {
    
    cout << "Enter an integer: " << endl;
    int value;
    cin >> value;
    
    int squareValue = square(value);
    
    cout << "The square of " << value << "is " << squareValue << endl;
    
   
    cout << "Enter a number :" << endl
        << " 1 to run square :" << endl\
        << " -1 to quit" << endl;
    
    int select;
    cin >> select;
    
    while (select != -1)
        
    {
        switch (select)
        {
            case 1 :
                problem1();
                break;
            case -1 :
                endProgram();
                break;
            default :
                cout << "Error" << endl;
    
                    
        }
        cout << "Enter a number :" << endl
        << " 1 to run square :" << endl\
        << " -1 to quit" << endl;
            
        }
    
    return 0;
}

