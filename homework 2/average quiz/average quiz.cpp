   //* Name: Minwossi Zerbo
   //* Student ID: 2498114 
   //* Date: 03/06/2014
   //* HW 2:Project 2
   //* Problem: Calculates the average of each quiz and output
   // in a specific design
   //* I certify this is my own work and code

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

int main(int argc, char *argv[])
{
    string name = "Name";
    string name_1 = "Minwossi";
    string name_2 = "Michael";
    string name_3 = "David";
    string quiz_1 = "Quiz 1";
    string quiz_2 = "Quiz 2";
    string quiz_3 = "Quiz 3";
    string quiz_4 = "Quiz 4";
    string average = "Average";
    int num1, num2, num3, num4;
    int num5, num6, num7, num8;
    int num9, num10, num11, num12;
    double average1, average2, average3, average4;
    cout << " Enter Minwossi's grades: " << endl;
    cin >> num1
        >> num2
        >> num3
        >> num4;
    cout << " Enter Michael's grades: " << endl;
    cin >> num5 
        >> num6
        >> num7
        >> num8;
    cout << " Enter David's grades: " << endl;
    cin >> num9
        >> num10
        >> num11
        >> num12;
     
     average1 = (num1 + num5 + num9)/ 3.0;
     average2 = (num2 + num6 + num10)/ 3.0;
     average3 = (num3 + num7 + num11)/ 3.0;
     average4 = (num4 + num8 + num12)/ 3.0; 
     
     cout << name << setw(8) << " "
          << quiz_1 << setw(3) << " "
          << quiz_2 << setw(3) << " "
          << quiz_3 << setw(3) << " "
          << quiz_4 << setw(3) << " " << endl;
          
     cout << setw(4) << left << "----" << setw(8) << " "
          << setw(6) << left << "------" << setw(3) << " "
          << setw(6) << left << "------" << setw(3) << " "
          << setw(6) << left << "------" << setw(3) << " "
          << setw(6) << left << "------" << setw(3) << " " << endl;
          
     cout << name_1 << setw(5) << " " 
          << setw(3)<< right << num1 << setw(6) << " "
          << setw(3)<< right << num2 << setw(6) << " "
          << setw(3)<< right << num3 << setw(6) << " "
          << setw(3)<< right << num4 << setw(6) << " " << endl;
          
     cout << name_2 << setw(6) << " " 
          << setw(3)<< right << num5 << setw(6) << " "
          << setw(3)<< right << num6 << setw(6) << " "
          << setw(3)<< right << num7 << setw(6) << " "
          << setw(3)<< right << num8 << setw(6) << " " << endl; 
           
     cout << name_3 << setw(8) << " " 
          << setw(3)<< right << num9 << setw(6) << " "
          << setw(3)<< right << num10 << setw(6) << " "
          << setw(3)<< right << num11 << setw(6) << " "
          << setw(3)<< right << num12 << setw(6) << " " << endl << endl ; 
          
     cout << average << setw(6) << " " 
          << setprecision(2)<< fixed << setw(3)<< left << average1 << setw(6) << " "
          << setprecision(2)<< setw(0)<< left << average2 << setw(6) << " "
          << setprecision(2)<< setw(0)<< left << average3 << setw(6) << " "
          << setprecision(2)<< setw(0)<< left << average4 << setw(6) << " " << endl << endl ;    
           
     
     
    system("PAUSE");
    return EXIT_SUCCESS;
}
