/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 3, 2014, 4:25 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;


/*
 * 
 */
int main(int argc, char** argv) {
 
    const double A = 1609.344;
    const double B = 3.281;
    const double C = 39.3701;
    double miles, feet, inches, measurement;
    
    cout << "Enter your measurement please: ";
    cin >> measurement;
    
    miles = measurement / A;
    feet = measurement * B;
    inches = measurement * C;
    
    
    cout << setprecision (2) << fixed << "MILES = " << miles << endl;
    
    cout <<  "FEET = " << feet << endl;
    
    cout << "INCHES = " << inches << endl;
    
    return 0;
}

