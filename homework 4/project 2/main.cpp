    //* Name: Minwossi Zerbo
   //* Student ID: 2498114 
   //* Date: 03/19/2014
   //* HW 4:Project 2
   //* Problem: Use project 1 and make it selects 10 random numbers
   //* I certify this is my own work and code
#include <cstdlib>
#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    int numPositive = 0;
    int numNegative = 0;
    double average1;
    double average2;
    
    for (int i = 0; i < 10; i++)
    {
        srand(time(0));
        int num = (rand () % 200) - 100;
        
        
        if ( num > 0) 
        {
             numPositive += num;
             }
        else 
        {
             numNegative += num;
             }
    }
    int count = numPositive;
    int count1 = numNegative;
  
    cout << count 
         << count1 << endl;
    cout << "The sum of your positive numbers is: " << numPositive << endl;
    cout << "The sum of your negative numbers is: " << numNegative << endl;
    
    average1 = numPositive / 10.00;
    average2 = numNegative / 10.00;
             
    cout << "The average of your positive numbers is: " << average1 <<endl;
    cout << "The average of your negative numbers is: " << average2 << endl;
    
    system("PAUSE");
    return EXIT_SUCCESS;
}
