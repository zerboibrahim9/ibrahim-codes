/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 3, 2014, 3:27 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip> //allows for output manipulation

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    //create a constant variable so i can't change it.
    
    const double num = 12.3294;
    
    // Illegal code
    //num = 12;
    
    //default right justify
    cout << setw(10) << num <<num << endl;
    
    // one fix
    //cout << setw(10); cout << left << num <<num << endl;

    //fix number 2 with << brackets
    cout << setw(10); cout << left << num <<num << endl;
    
    //2 digits
    cout << setprecision(2);
    cout << setw(10); cout << left << num <<num << endl;
    
    // 3 decimal places
    // fixed sets precision to decimal
    cout << setprecision(3) << fixed;
    cout << setw(10); cout << left << num <<num << endl;
    
    //set fill
    cout << setfill('=');
    cout << setw(10) << num << num << endl;
    
    //using ttwo setws
    cout << setw(10) << num << setw(10)<< right << num << endl;
    
    //changes setfill
    cout << setfill(' ');
    cout << setw(10) << num << setw(10)<< right << num << endl;
    
    return 0;
}

