/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 28, 2014, 4:39 PM
 */

#include <cstdlib>
#include <iostream>
#include <vector>
#include <fstream>

using namespace std;


/*
 * 
 */
void output(const vector<int> &v)
{
    for (int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
    
}

int main(int argc, char** argv) {
    
    ifstream infile;
    
    infile.open("data.dat");
    
    if(!infile)
    {
        cout << "The file doesn't exist. SORRY" << endl;
    }
    int num;
    vector<int>numbers;
    
    while(infile >> num )
    {
        numbers.push_back(num);
        
    }

    output(numbers);
    return 0;
}

