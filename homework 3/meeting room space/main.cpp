#include <cstdlib>
#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    int roomspace;
    int numPeople;
    
    cout << " Enter the capacity of your room: " << endl;
    cin >> roomspace;
    cout << " Your meeting room capacity is " << roomspace << endl;
    
    cout << " Enter the number of peoples attending the meeting: " << endl;
    cin >> numPeople;
    
    if (numPeople <= roomspace)
    {
      int remainingplace = roomspace - numPeople;
      cout << "Your meeting is authorized and scheduled,"
           << " and you have " << remainingplace
           << " places for more people." << endl;           
    }
    else 
    {
         int peopleChased = numPeople - roomspace;
         cout << " Your meeting is not authorized because"
              << " you have too many people "
              << " and the room  is in violation of the fire "
              << "law regulation." << endl
              << " To schedule"
              << " your meeting you have to"
              << " remove " << peopleChased
              << " persons from your list." << endl;
         
    }
    system("PAUSE");
    return EXIT_SUCCESS;
}
