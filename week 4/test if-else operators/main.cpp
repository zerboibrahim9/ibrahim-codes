/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 5, 2014, 3:23 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    string name = "Minwossi Zerbo";
    name.substr(2,5);
    
    cout << name.substr(2,5)<< endl ;
    
     cout << name.substr(2,7) << endl;
     
     //using find member function
     cout << name.find("o") << endl;
     
     //string not located in name
     cout << name.find("uijijssjsojjd") << endl;

     int loc = name.find("uijijssjsojjd");
     
     //increment it by one
     loc++;
     // should output/reset to 0
     cout << "loc:" << loc << endl;
     
     // output the lenght and the size of the name
     cout << "lenght: " << name.length()<< endl;
     
     // If statements
     
     int x = 10;
     cout << " X before IF: " << x << endl;
     
     if (x > 5)
     { 
         x += 5; // if x is greater than 5
     }
     
     cout << " X after IF: " << x << endl;
     
     int y = 5;
     
      cout << " Y before IF: " << y << endl;
     
     if (y < 5)
     {
         y += 5;
        
     }
     else
     {
         y -= 5;
     
     }
     cout << " Y after IF: " << y << endl;
     
     
    return 0;
    
}

