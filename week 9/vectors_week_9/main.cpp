/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 23, 2014, 3:36 PM
 */

#include <cstdlib>
#include <iostream>
#include <vector>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    // Gte 10 numbers from the user and store in the vector
    // Method 1. Empty vector
    
    vector<int> emptyValues;
    
    //Print the vector to the screen before adding values
    for(int i = 0; i < emptyValues.size(); i++)
    {
        cout << "Before vector" << endl;
        cout << emptyValues[i] << endl;
    }
    for(int i = 0; i < 10; i++)
    {
        cout << "Enter a number" << endl;
        int num;
        cin >> num;
        
        // Push inserts the value to the end of the vector
        emptyValues.push_back(num);
    
    }
    
    for(int i = 0; i < emptyValues.size(); i++)
    {
        cout << "After vector" << endl;
        cout << emptyValues[i] << endl;
    }
    
    // Method 2. known size
    
    vector<int> values(10);
    
    //Creating a vector with values size 10
    
    
   for(int i = 0; i < values.size(); i++)
    {
       cout << "Before vector" << endl;
        cout << values[i] << endl;
    }
    
    for(int i = 0; i < 10; i++)
    {
        cout << "Enter a number" << endl;
        int num;
        cin >> num;
        values[i]= num;
    
    }
    for(int i = 0; i < values.size(); i++)
    {
        cout << "After vector" << endl;
        cout << values[i] << endl;
    }
    return 0;
}

