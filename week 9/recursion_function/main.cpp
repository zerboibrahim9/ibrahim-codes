/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 23, 2014, 4:13 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */

// recur
int recurFib(int sequence)
{
    // base case
    cout << "Fibonacci Sequence: " << sequence << endl;
    if (sequence <= 2) return 1;
    // recursive call
    else
        return recurFib(sequence - 1) + recurFib(sequence - 2);
}

// non recur
int nonRecurFibo(int sequence)
{
    int sum = 0;
    int first = 1;
    int second = 1;
    
    if (sequence <= 2)
        return first;
    
    for (int i = 2; i< sequence; i++)
    {
        sum = first + second;
        first = second;
        second = sum;
    
    }
    return sum;

}
int main(int argc, char** argv) {

    while(true)
    {
            
    cout << "Please enter a number: " << endl;
    int num;
    cin >> num;
    
    cout << "Fibonacci of: " << num << " is: "
         << nonRecurFibo(num) << endl;
//    int fibo = recurFib(num);
    
//    cout << "Fibonacci of : " << num << " is: " << fibo << endl;
    }
    return 0;
}

