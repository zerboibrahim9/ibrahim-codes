/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 12, 2014, 4:15 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    // get the amout owed by user
    cout << "Enter the amout owed: " << endl;
    double owe;
    cin >> owe;
    // get the tender
    cout << "Enter the amout paid: " << endl;
    double tender;
    cin >> tender;
    
    //get change from the user
    
    double change = tender - owe;
    
    change *= 100;
    int changeInPennies = static_cast<int>(change + .5);
    cout << "Change in pennies: "
            << changeInPennies << endl;
    
    const int dollar = 100;
    const int quarter = 25;
    const int dime = 10;
    const int nickel = 5;
    const int penny = 1;
    
    // calculate the number of dollars
    int numdollars = changeInPennies / dollar;
    
    // get remaining pennies
    
    changeInPennies %= dollar;
    
    // quarters
    int numquarters = changeInPennies / quarter;
    changeInPennies %= quarter;
    
    //dimes
    int numdimes = changeInPennies / dime;
    changeInPennies %= dime;
    
    // nickels
    int numnickels = changeInPennies / nickel;
    changeInPennies %= nickel;
    
    // pennies
    int numpennies = changeInPennies;
    
    //output each amout
    
    cout << " Number of dollars: " << numdollars << endl
            << "Number of quarters: " << numquarters << endl
            << "Number of dime :  " << numdimes << endl
            << "Number of nickles: " << numnickels << endl
            << "Number of pennies: " << numpennies << endl;
    return 0;
    
}

